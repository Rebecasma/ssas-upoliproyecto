USE [control de inventario y proveedores OLAP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaFactConsulta]    

Script Date: 06/12/2013 15:34:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:      Rebeca Manzanares
-- Create date: 2013.06.08
-- Description:	Carga FactConsulta
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaFactConsulta]
AS
BEGIN

	Delete From dbo.FactConsulta;

	DBCC CHECKIDENT ('FactConsulta', reseed, 0);
	DBCC CHECKIDENT ('FactConsulta', reseed);

INSERT INTO [control de inventario y proveedores OLAP].[dbo].[FactConsulta]
           ([CodigoProductoKey]
           ,[ProveedoresKey]
           ,[ProductoKey]
 
           
Select   IDMovimiento 
        ,CodigoProducto
        ,Producto
        ,PrecioUnidad
        ,Recibido                        
        ,CantidadRecibida
        ,Solicitud   
        ,IdDetalleMovimiento
        ,IDArea  
                 
     From [Control de Inventario y Proveedores].[dbo].[FactConsulta] order by IdDetalleMovimiento; 
END

