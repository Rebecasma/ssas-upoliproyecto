
USE [control de inventario y proveedores OLAP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimDetalleMoviento]    

Script Date: 06/12/2013 15:34:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rebeca Manzanares
-- Create date: 2013.06.08
-- Description:	Carga Dimension Proveedores
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaDimProveedore]
AS
BEGIN

	Delete From dbo.DimDetalleMoviento;

	DBCC CHECKIDENT ('DimProveedore', reseed, 0);
	DBCC CHECKIDENT ('DimProveedore', reseed);

INSERT INTO [control de inventario y proveedores OLAP].[dbo].[DimProveedore]
            ([ProveedoresKey]
            ,[ProveedoresAlternateKey]
            ,[Nombre]
            ,[Telefono])
           
           
Select       IdProveedor
            ,Descripcion
            ,Nombre
            ,Direccion de la Empresa
            ,Departamento
            ,Pais
            ,CodigoPostal
            ,Telefono
            ,Email
                 
     From [Control de Inventario y Proveedores].[dbo].[Proveedore] order by IdDetalleMovimiento; 
END

