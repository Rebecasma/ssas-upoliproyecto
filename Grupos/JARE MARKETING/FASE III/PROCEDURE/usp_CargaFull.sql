USE [control de inventario y proveedores OLAP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaFull]    Script Date: 06/12/2013 15:35:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rebeca Manzanares
-- Create date: 2013.06.08
-- Description:	Carga Completa de control de inventario y proveedores OLAP
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaFullDW]
AS
BEGIN
	
	
	--1. Eliminamos Datos de FactNotas
	Delete From dbo.FactConsulta;
	
	
	exec dbo.usp_CargaDimDetalleMoviento;
	exec dbo.usp_CargaDimProducto;
	exec dbo.usp_CargaDimProveedore;
	exec dbo.usp_CargaDimEstudiante;
	exec dbo.usp_CargaFactConsulta
	
END
