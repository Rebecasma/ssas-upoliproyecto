USE [control de inventario y proveedores OLAP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimDetalleMoviento]    

Script Date: 06/12/2013 15:34:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rebeca Manzanares
-- Create date: 2013.06.08
-- Description:	Carga Dimension Producto
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaDimProducto]
AS
BEGIN

	Delete From dbo.DimProducto;

	DBCC CHECKIDENT ('DimProducto', reseed, 0);
	DBCC CHECKIDENT ('DimProducto', reseed);

INSERT INTO [control de inventario y proveedores OLAP].[dbo].[DimProducto]
           ([CodigoProductoKey]
           ,[CodigoProductoAlternate]
           ,[Categoria]
           ,[Nombre]
           ,[Descripcion])
           
Select    CodigoProducto          
         ,Categoria                
         ,IdProducto          
         ,Fecha de Ingreso 
          Existencias Minimas    
         ,Suspendidos              
         ,IdUnidad                 
         ,IdCategoria              
         ,IdProveddor             
         ,Nombre                  
         ,Descripcion              
         ,Ubicacion en Deposito
         ,Costo por Unidad   
                 
     From [Control de Inventario y Proveedores].[dbo].[DimProducto] order by IdProducto ; 
END

