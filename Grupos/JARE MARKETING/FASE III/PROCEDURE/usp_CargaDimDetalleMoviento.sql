
USE [control de inventario y proveedores OLAP]
GO
/****** Object:  StoredProcedure [dbo].[usp_CargaDimDetalleMoviento]    

Script Date: 06/12/2013 15:34:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Rebeca Manzanares
-- Create date: 2013.06.08
-- Description:	Carga Dimension Detalle de Movimiento
-- =============================================
ALTER PROCEDURE [dbo].[usp_CargaDimDetalleMoviento]
AS
BEGIN

	Delete From dbo.DimDetalleMoviento;

	DBCC CHECKIDENT ('DimDetalleMoviento', reseed, 0);
	DBCC CHECKIDENT ('DimDetalleMoviento', reseed);

INSERT INTO [control de inventario y proveedores OLAP].[dbo].[DimDetalleMoviento]
           ([ProductoKey]
           ,[ProductoAlternate])
 
           
Select   IDMovimiento 
        ,CodigoProducto
        ,Producto
        ,PrecioUnidad
        ,Recibido                        
        ,CantidadRecibida
        ,Solicitud   
        ,IdDetalleMovimiento
        ,IDArea  
                 
     From [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento] order by IdDetalleMovimiento; 
END

