/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      evenor
 * Project :      modelo inicial del trabajo olap.dm1
 * Author :       Evenor
 *
 * Date Created : Monday, June 10, 2013 20:29:35
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE [control de inventario y proveedores OLAP]
go
USE [control de inventario y proveedores OLAP]
go
/* 
 * TABLE: DimDetalleMoviento 
 */

CREATE TABLE DimDetalleMoviento(
    ProductoKey          char(10)    NOT NULL,
    ProductoAlternate    char(30)    NOT NULL,
    CONSTRAINT PK5 PRIMARY KEY NONCLUSTERED (ProductoKey)
)
go



IF OBJECT_ID('DimDetalleMoviento') IS NOT NULL
    PRINT '<<< CREATED TABLE DimDetalleMoviento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimDetalleMoviento >>>'
go

/* 
 * TABLE: DimProducto 
 */

CREATE TABLE DimProducto(
    CodigoProductoKey          numeric(10, 0)    IDENTITY(1,1),
    CodigoProductoAlternate    numeric(10, 0)    NOT NULL,
    Categoria                  nchar(10)         NOT NULL,
    Nombre                     nvarchar(60)      NOT NULL,
    Descripcion                nvarchar(60)      NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY NONCLUSTERED (CodigoProductoKey)
)
go



IF OBJECT_ID('DimProducto') IS NOT NULL
    PRINT '<<< CREATED TABLE DimProducto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimProducto >>>'
go

/* 
 * TABLE: DimProveedore 
 */

CREATE TABLE DimProveedore(
    ProveedoresKey             numeric(10, 0)    IDENTITY(1,1),
    ProveedoresAlternateKey    numeric(10, 0)    NOT NULL,
    Nombre                     nvarchar(60)      NOT NULL,
    Telefono                   numeric(10, 0)    NOT NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (ProveedoresKey)
)
go



IF OBJECT_ID('DimProveedore') IS NOT NULL
    PRINT '<<< CREATED TABLE DimProveedore >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DimProveedore >>>'
go

/* 
 * TABLE: FactConsulta 
 */

CREATE TABLE FactConsulta(
    CodigoProductoKey    numeric(10, 0)    NOT NULL,
    ProveedoresKey       numeric(10, 0)    NOT NULL,
    ProductoKey          char(10)          NOT NULL,
    CONSTRAINT PK10 PRIMARY KEY NONCLUSTERED (CodigoProductoKey, ProveedoresKey, ProductoKey)
)
go



IF OBJECT_ID('FactConsulta') IS NOT NULL
    PRINT '<<< CREATED TABLE FactConsulta >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE FactConsulta >>>'
go

/* 
 * TABLE: FactConsulta 
 */

ALTER TABLE FactConsulta ADD CONSTRAINT RefDimDetalleMoviento1 
    FOREIGN KEY (ProductoKey)
    REFERENCES DimDetalleMoviento(ProductoKey)
go

ALTER TABLE FactConsulta ADD CONSTRAINT RefDimProducto2 
    FOREIGN KEY (CodigoProductoKey)
    REFERENCES DimProducto(CodigoProductoKey)
go

ALTER TABLE FactConsulta ADD CONSTRAINT RefDimProveedore3 
    FOREIGN KEY (ProveedoresKey)
    REFERENCES DimProveedore(ProveedoresKey)
go


