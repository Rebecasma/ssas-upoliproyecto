/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      p
 * Project :      Model1.DM1
 * Author :       Admin
 *
 * Date Created : Saturday, May 25, 2013 22:03:52
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE [Iventario y Proveedores]
go
USE [Iventario y Proveedores]
go
/* 
 * TABLE: Area 
 */

CREATE TABLE Area(
    IDArea         numeric(10, 0)    NOT NULL,
    Area           char(10)          NULL,
    Descripcion    nvarchar(100)     NULL,
    Suependido     nvarchar(50)      NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (IDArea)
)
go



IF OBJECT_ID('Area') IS NOT NULL
    PRINT '<<< CREATED TABLE Area >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Area >>>'
go

/* 
 * TABLE: [Cargo de Empleado] 
 */

CREATE TABLE [Cargo de Empleado](
    IdCargoEmpleado    numeric(10, 0)    NOT NULL,
    [Cargo Empleado]   nvarchar(25)      NULL,
    CONSTRAINT PK8 PRIMARY KEY NONCLUSTERED (IdCargoEmpleado)
)
go



IF OBJECT_ID('Cargo de Empleado') IS NOT NULL
    PRINT '<<< CREATED TABLE Cargo de Empleado >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Cargo de Empleado >>>'
go

/* 
 * TABLE: Categoria 
 */

CREATE TABLE Categoria(
    IdCategoria         numeric(10, 0)    NOT NULL,
    Categoria           char(20)          NULL,
    Descripcion         numeric(80, 0)    NULL,
    Suspendido          char(10)          NULL,
    CuentaInventario    char(10)          NULL,
    CONSTRAINT PK5 PRIMARY KEY NONCLUSTERED (IdCategoria)
)
go



IF OBJECT_ID('Categoria') IS NOT NULL
    PRINT '<<< CREATED TABLE Categoria >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Categoria >>>'
go

/* 
 * TABLE: [Detalle de Movimiento] 
 */

CREATE TABLE [Detalle de Movimiento](
    IDMovimiento             numeric(10, 0)    NOT NULL,
    CodigoProducto           numeric(10, 0)    NOT NULL,
    Producto                 char(30)          NOT NULL,
    PrecioUnidad             numeric(10, 0)    NULL,
    Recibido                 char(10)          NULL,
    CantidadRecivida         numeric(10, 0)    NULL,
    [Recibido Parcialmente]  numeric(10, 0)    NULL,
    Solicitud                numeric(10, 0)    NULL,
    [Retiro de Material]     numeric(10, 0)    NULL,
    IDArea                   numeric(10, 0)    NULL,
    Id                       numeric(10, 0)    NULL,
    IdMovimiento             numeric(10, 0)    NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (IDMovimiento)
)
go



IF OBJECT_ID('Detalle de Movimiento') IS NOT NULL
    PRINT '<<< CREATED TABLE Detalle de Movimiento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Detalle de Movimiento >>>'
go

/* 
 * TABLE: DetalleProducto 
 */

CREATE TABLE DetalleProducto(
    Id                numeric(10, 0)    NOT NULL,
    Cantidad          numeric(10, 0)    NULL,
    CodifoProducto    numeric(10, 0)    NULL,
    CodigoProducto    numeric(10, 0)    NULL,
    CONSTRAINT PK11 PRIMARY KEY NONCLUSTERED (Id)
)
go



IF OBJECT_ID('DetalleProducto') IS NOT NULL
    PRINT '<<< CREATED TABLE DetalleProducto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE DetalleProducto >>>'
go

/* 
 * TABLE: Empleados 
 */

CREATE TABLE Empleados(
    IdEmpleado            numeric(10, 0)    NOT NULL,
    Nombre                nvarchar(40)      NULL,
    Apellidos             nvarchar(40)      NULL,
    Telefono              numeric(10, 0)    NULL,
    [Correo Electronico]  nvarchar(60)      NULL,
    IdCargo               numeric(10, 0)    NOT NULL,
    IdCargoEmpleado       numeric(10, 0)    NOT NULL,
    CONSTRAINT PK9 PRIMARY KEY NONCLUSTERED (IdEmpleado)
)
go



IF OBJECT_ID('Empleados') IS NOT NULL
    PRINT '<<< CREATED TABLE Empleados >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Empleados >>>'
go

/* 
 * TABLE: Movimiento 
 */

CREATE TABLE Movimiento(
    IdMovimiento        numeric(10, 0)    NOT NULL,
    IdEmpleado          numeric(10, 0)    NOT NULL,
    IdArea              char(10)          NULL,
    IdTipoMovimiento    numeric(10, 0)    NOT NULL,
    FechaMovimiento     date              NULL,
    NoSolicitud         numeric(10, 0)    NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (IdMovimiento)
)
go



IF OBJECT_ID('Movimiento') IS NOT NULL
    PRINT '<<< CREATED TABLE Movimiento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Movimiento >>>'
go

/* 
 * TABLE: Producto 
 */

CREATE TABLE Producto(
    CodigoProducto           numeric(10, 0)    NOT NULL,
    IdProducto               numeric(10, 0)    NOT NULL,
    [Fecha de Ingreso]       date              NOT NULL,
    [Existencias Minimas]    char(10)          NULL,
    Suspendidos              char(10)          NULL,
    IdUnidad                 numeric(10, 0)    NOT NULL,
    IdCategoria              numeric(10, 0)    NOT NULL,
    IdProveddor              char(10)          NOT NULL,
    Bombre                   nvarchar(60)      NOT NULL,
    Descripcion              nvarchar(60)      NOT NULL,
    [Ubicacion en Deposito]  nvarchar(70)      NOT NULL,
    [Costo por Unidad]       numeric(10, 0)    NOT NULL,
    IdProveedor              numeric(10, 0)    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (CodigoProducto)
)
go



IF OBJECT_ID('Producto') IS NOT NULL
    PRINT '<<< CREATED TABLE Producto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Producto >>>'
go

/* 
 * TABLE: Proveedores 
 */

CREATE TABLE Proveedores(
    IdProveedor                numeric(10, 0)    NOT NULL,
    [Direccion Web]            nvarchar(100)     NULL,
    Descripcion                nvarchar(90)      NULL,
    Nombre                     nvarchar(60)      NULL,
    [Direccion de la Empresa]  nvarchar(100)     NULL,
    Departamento               char(10)          NULL,
    Pais                       char(10)          NULL,
    CodigoPostal               char(10)          NULL,
    Telefono                   numeric(25, 0)    NULL,
    Email                      nvarchar(80)      NOT NULL,
    CONSTRAINT PK10 PRIMARY KEY NONCLUSTERED (IdProveedor)
)
go



IF OBJECT_ID('Proveedores') IS NOT NULL
    PRINT '<<< CREATED TABLE Proveedores >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Proveedores >>>'
go

/* 
 * TABLE: [Tipo de Movimiento] 
 */

CREATE TABLE [Tipo de Movimiento](
    IdTipoMovimiento    numeric(10, 0)    NOT NULL,
    Movimiento          nvarchar(40)      NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (IdTipoMovimiento)
)
go



IF OBJECT_ID('Tipo de Movimiento') IS NOT NULL
    PRINT '<<< CREATED TABLE Tipo de Movimiento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Tipo de Movimiento >>>'
go

/* 
 * TABLE: Unidad 
 */

CREATE TABLE Unidad(
    IdUnidad       numeric(10, 0)    NOT NULL,
    Unidad         char(10)          NULL,
    Descripcion    nvarchar(80)      NULL,
    CONSTRAINT PK6 PRIMARY KEY NONCLUSTERED (IdUnidad)
)
go



IF OBJECT_ID('Unidad') IS NOT NULL
    PRINT '<<< CREATED TABLE Unidad >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Unidad >>>'
go

/* 
 * TABLE: [Detalle de Movimiento] 
 */

ALTER TABLE [Detalle de Movimiento] ADD CONSTRAINT RefArea2 
    FOREIGN KEY (IDArea)
    REFERENCES Area(IDArea)
go

ALTER TABLE [Detalle de Movimiento] ADD CONSTRAINT RefDetalleProducto3 
    FOREIGN KEY (Id)
    REFERENCES DetalleProducto(Id)
go

ALTER TABLE [Detalle de Movimiento] ADD CONSTRAINT RefMovimiento16 
    FOREIGN KEY (IdMovimiento)
    REFERENCES Movimiento(IdMovimiento)
go


/* 
 * TABLE: DetalleProducto 
 */

ALTER TABLE DetalleProducto ADD CONSTRAINT RefProducto7 
    FOREIGN KEY (CodigoProducto)
    REFERENCES Producto(CodigoProducto)
go


/* 
 * TABLE: Empleados 
 */

ALTER TABLE Empleados ADD CONSTRAINT RefCargo_de_Empleado17 
    FOREIGN KEY (IdCargoEmpleado)
    REFERENCES [Cargo de Empleado](IdCargoEmpleado)
go


/* 
 * TABLE: Movimiento 
 */

ALTER TABLE Movimiento ADD CONSTRAINT RefTipo_de_Movimiento13 
    FOREIGN KEY (IdTipoMovimiento)
    REFERENCES [Tipo de Movimiento](IdTipoMovimiento)
go

ALTER TABLE Movimiento ADD CONSTRAINT RefEmpleados15 
    FOREIGN KEY (IdEmpleado)
    REFERENCES Empleados(IdEmpleado)
go


/* 
 * TABLE: Producto 
 */

ALTER TABLE Producto ADD CONSTRAINT RefCategoria8 
    FOREIGN KEY (IdCategoria)
    REFERENCES Categoria(IdCategoria)
go

ALTER TABLE Producto ADD CONSTRAINT RefProveedores9 
    FOREIGN KEY (IdProveedor)
    REFERENCES Proveedores(IdProveedor)
go

ALTER TABLE Producto ADD CONSTRAINT RefUnidad10 
    FOREIGN KEY (IdUnidad)
    REFERENCES Unidad(IdUnidad)
go


