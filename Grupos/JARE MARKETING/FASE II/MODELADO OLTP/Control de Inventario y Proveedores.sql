/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      p
 * Project :      Modelado de control de iventario y proveedores.dm1
 * Author :       Admin
 *
 * Date Created : Sunday, June 09, 2013 14:24:04
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE [Control de Inventario y Proveedores]
go
USE [Control de Inventario y Proveedores]
go
/* 
 * TABLE: Area 
 */

CREATE TABLE Area(
    IDArea         numeric(10, 0)    NOT NULL,
    Area           char(10)          NULL,
    Descripcion    nvarchar(100)     NULL,
    Suspendido     nvarchar(50)      NULL,
    CONSTRAINT PK2 PRIMARY KEY NONCLUSTERED (IDArea)
)
go



IF OBJECT_ID('Area') IS NOT NULL
    PRINT '<<< CREATED TABLE Area >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Area >>>'
go

/* 
 * TABLE: [Detalle de Movimiento] 
 */

CREATE TABLE [Detalle de Movimiento](
    IDMovimiento           numeric(10, 0)    NOT NULL,
    CodigoProducto         numeric(10, 0)    NULL,
    Producto               char(30)          NOT NULL,
    PrecioUnidad           numeric(10, 0)    NULL,
    Recibido               char(10)          NULL,
    CantidadRecibida       numeric(10, 0)    NULL,
    Solicitud              numeric(10, 0)    NULL,
    IdDetalleMovimiento    numeric(10, 0)    NOT NULL,
    IDArea                 numeric(10, 0)    NULL,
    CONSTRAINT PK1 PRIMARY KEY NONCLUSTERED (IDMovimiento)
)
go



IF OBJECT_ID('Detalle de Movimiento') IS NOT NULL
    PRINT '<<< CREATED TABLE Detalle de Movimiento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Detalle de Movimiento >>>'
go

/* 
 * TABLE: Empleados 
 */

CREATE TABLE Empleados(
    IdEmpleado            numeric(10, 0)    NOT NULL,
    Nombre                nvarchar(40)      NULL,
    Apellidos             nvarchar(40)      NULL,
    Telefono              numeric(10, 0)    NULL,
    [Correo Electronico]  nvarchar(60)      NULL,
    Cargo                 char(20)          NULL,
    Area                  char(10)          NULL,
    CONSTRAINT PK9 PRIMARY KEY NONCLUSTERED (IdEmpleado)
)
go



IF OBJECT_ID('Empleados') IS NOT NULL
    PRINT '<<< CREATED TABLE Empleados >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Empleados >>>'
go

/* 
 * TABLE: Movimiento 
 */

CREATE TABLE Movimiento(
    IdDetalleMovimiento    numeric(10, 0)    NOT NULL,
    IdEmpleado             numeric(10, 0)    NOT NULL,
    IdArea                 char(10)          NULL,
    IdTipoMovimiento       numeric(10, 0)    NOT NULL,
    FechaMovimiento        date              NULL,
    NoSolicitud            numeric(10, 0)    NULL,
    CONSTRAINT PK4 PRIMARY KEY NONCLUSTERED (IdDetalleMovimiento)
)
go



IF OBJECT_ID('Movimiento') IS NOT NULL
    PRINT '<<< CREATED TABLE Movimiento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Movimiento >>>'
go

/* 
 * TABLE: Producto 
 */

CREATE TABLE Producto(
    CodigoProducto           numeric(10, 0)    NOT NULL,
    Categoria                char(30)          NULL,
    IdProducto               numeric(10, 0)    NOT NULL,
    [Fecha de Ingreso]       date              NOT NULL,
    [Existencias Minimas]    char(10)          NULL,
    Suspendidos              char(10)          NULL,
    IdUnidad                 numeric(10, 0)    NOT NULL,
    IdCategoria              nchar(10)         NOT NULL,
    IdProveddor              char(10)          NOT NULL,
    Nombre                   nvarchar(60)      NOT NULL,
    Descripcion              nvarchar(60)      NOT NULL,
    [Ubicacion en Deposito]  nvarchar(70)      NOT NULL,
    [Costo por Unidad]       numeric(10, 0)    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY NONCLUSTERED (CodigoProducto)
)
go



IF OBJECT_ID('Producto') IS NOT NULL
    PRINT '<<< CREATED TABLE Producto >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Producto >>>'
go

/* 
 * TABLE: [Producto Proveedores] 
 */

CREATE TABLE [Producto Proveedores](
    CodigoProducto    numeric(10, 0)    NOT NULL,
    IdProveedor       numeric(10, 0)    NOT NULL,
    CONSTRAINT PK12 PRIMARY KEY NONCLUSTERED (CodigoProducto, IdProveedor)
)
go



IF OBJECT_ID('Producto Proveedores') IS NOT NULL
    PRINT '<<< CREATED TABLE Producto Proveedores >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Producto Proveedores >>>'
go

/* 
 * TABLE: Proveedores 
 */

CREATE TABLE Proveedores(
    IdProveedor                numeric(10, 0)    NOT NULL,
    Descripcion                nvarchar(90)      NULL,
    Nombre                     nvarchar(60)      NULL,
    [Direccion de la Empresa]  nvarchar(100)     NULL,
    Departamento               char(10)          NULL,
    Pais                       char(10)          NULL,
    CodigoPostal               char(10)          NULL,
    Telefono                   numeric(25, 0)    NULL,
    Email                      nvarchar(80)      NOT NULL,
    CONSTRAINT PK10 PRIMARY KEY NONCLUSTERED (IdProveedor)
)
go



IF OBJECT_ID('Proveedores') IS NOT NULL
    PRINT '<<< CREATED TABLE Proveedores >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Proveedores >>>'
go

/* 
 * TABLE: [Tipo de Movimiento] 
 */

CREATE TABLE [Tipo de Movimiento](
    IdTipoMovimiento    numeric(10, 0)    NOT NULL,
    Movimiento          nvarchar(40)      NULL,
    CONSTRAINT PK7 PRIMARY KEY NONCLUSTERED (IdTipoMovimiento)
)
go



IF OBJECT_ID('Tipo de Movimiento') IS NOT NULL
    PRINT '<<< CREATED TABLE Tipo de Movimiento >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Tipo de Movimiento >>>'
go

/* 
 * TABLE: [Detalle de Movimiento] 
 */

ALTER TABLE [Detalle de Movimiento] ADD CONSTRAINT RefMovimiento16 
    FOREIGN KEY (IdDetalleMovimiento)
    REFERENCES Movimiento(IdDetalleMovimiento)
go

ALTER TABLE [Detalle de Movimiento] ADD CONSTRAINT RefProducto18 
    FOREIGN KEY (CodigoProducto)
    REFERENCES Producto(CodigoProducto)
go

ALTER TABLE [Detalle de Movimiento] ADD CONSTRAINT RefArea22 
    FOREIGN KEY (IDArea)
    REFERENCES Area(IDArea)
go


/* 
 * TABLE: Movimiento 
 */

ALTER TABLE Movimiento ADD CONSTRAINT RefTipo_de_Movimiento13 
    FOREIGN KEY (IdTipoMovimiento)
    REFERENCES [Tipo de Movimiento](IdTipoMovimiento)
go

ALTER TABLE Movimiento ADD CONSTRAINT RefEmpleados15 
    FOREIGN KEY (IdEmpleado)
    REFERENCES Empleados(IdEmpleado)
go


/* 
 * TABLE: [Producto Proveedores] 
 */

ALTER TABLE [Producto Proveedores] ADD CONSTRAINT RefProveedores24 
    FOREIGN KEY (IdProveedor)
    REFERENCES Proveedores(IdProveedor)
go

ALTER TABLE [Producto Proveedores] ADD CONSTRAINT RefProducto23 
    FOREIGN KEY (CodigoProducto)
    REFERENCES Producto(CodigoProducto)
go


