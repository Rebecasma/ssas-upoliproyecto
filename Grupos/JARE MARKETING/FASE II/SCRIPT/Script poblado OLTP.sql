
Select * from dbo.Area

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3235','Habitaciones','Closets','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3236','Comunes','Recepcion','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3237','Recreacion','Spa','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3238','Servicio','Servicio Administrativo','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3239','Habitaciones','Terraza','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3240','Comunes','Servicio de A & B','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3241','Recreacion','Sala de Juegos','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3242','Servicio','Cocina','No');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Area]([IDArea],[Area],[Descripcion],[Suspendido])VALUES('3243','Habitaciones','Regadera','No');

Select * from [dbo].[Detalle de Movimiento]

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9875','75635','Cajas fuerte','150','16','2400','SI','43457','3234');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9876','75636','Espejos','151','32','4832','SI','43458','3235');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9877','75637','Toallas','152','11','1672','SI','43459','3236');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9878','75638','Cerraduras','153','8','1224','SI','43460','3237');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9879','75639','Televisores','154','4','616','SI','43461','3238');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9880','75640','Minibarres','155','2','310','SI','43462','3239');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9881','75641','Complementos','156','4','624','SI','43463','3240');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9882','75642','Papeleras de ba�o','157','10','1570','SI','43464','3241');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9883','75643','Secadoras de cabello','158','2','316','SI','43465','3242');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Detalle de Movimiento]([IDMovimiento],[CodigoProducto],[Producto],[PrecioUnidad],[Recibido],[CantidadRecibida],[Solicitud],[IdDetalleMovimiento],[IDArea])VALUES('9884','75644','Secamanos','159','24','3816','SI','43466','3243');

Select * from dbo.Empleados

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('86','Juan','Acevedo','22679644','juan_acevedo@hotmail.com','Servicio','Servicio');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('87','Luana','Cruz','22005276','luana_cruz@outlook.com','Habitaciones','Habitaciones');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('88','Susana','Lopez','28753753','susana-lopez@hotmail.com','Comunes','Comunes');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('89','Cristhiam','Sotomayor','23326533','cristhiam2375@yahoo.com','Recreacion','Recreacion');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('90','Jose','Castellon','20376537','josecastellon@gmail.com','Servicio','Servicio');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('91','Pedro','Escobar','27635786','pedro_escobar@yahoo.com','Habitaciones','Habitaciones');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('92','Andres','Hernandez','28754835','hernandez_andres@yahoo.es','Comunes','Comunes');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('93','Gabriela','Flores','23655372','flores_gabriela@hotmail.com','Recreacion','Recreacion');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('94','Betty','Aguilar','23655433','betty_tatty@yahoo.es','Servicio','Servicio'):
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Empleados]([IdEmpleado],[Nombre],[Apellidos],[Telefono],[Correo Electronico],[Cargo],[Area])VALUES('95','Alejandra','Rodriguez','27654545','alejandra_s@gmail.com','Habitaciones','Habitaciones');



Select * from dbo.Movimiento

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9875','86','3234','75635','12072013','43457');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9876','87','3235','75636','12072013','43458');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9877','88','3236','75637','12072013','43459');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9878','89','3237','75638','12072013','43460');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9879','90','3238','75639','12072013','43461');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9880','91','3239','75640','12072013','43462');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9881','92','3240','75641','12072013','43463');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9882','93','3241','75642','12072013','43464');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9883','94','3242','75643','12072013','43465');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Movimiento]([IdDetalleMovimiento],[IdEmpleado],[IdArea],[IdTipoMovimiento],[FechaMovimiento],[NoSolicitud])VALUES('9884','95','3243','75644','12072013','43466');

Select * from dbo.Producto

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75635','Servicio','9875','10/05/2012','76','2','3234','9875','1','Cajas fuerte','Servicio','16','150');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75636','Habitaciones','9876','10/05/2013','77','3','3235','9876','2','Espejos','Habitaciones','32','151');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75637','Comunes','9877','10/05/2014','78','4','3236','9877','3','Toallas','Comunes','11','152');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75638','Recreacion','9878','10/05/2015','79','5','3237','9878','4','Cerraduras','Recreacion','8','153');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75639','Servicio','9879','10/05/2016','80','6','3238','9879','5','Televisores','Servicio','4','154');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75640','Habitaciones','9880','10/05/2017','81','7','3239','9880','6','Minibarres','Habitaciones','2','155');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75641','Comunes','9881','10/05/2018','82','8','3240','9881','7','Complementos','Comunes','4','156');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75642','Recreacion','9882','10/05/2019','83','9','3241','9882','8','Papeleras de ba�o','Recreacion','10','157');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75643','Servicio','9883','10/05/2020','84','10','3242','9883','9','Secadoras de cabello','Servicio','2','158');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto]([CodigoProducto],[Categoria],[IdProducto],[Fecha de Ingreso],[Existencias Minimas],[Suspendidos],[IdUnidad],[IdCategoria],[IdProveddor],[Nombre],[Descripcion],[Ubicacion en Deposito],[Costo por Unidad])VALUES ('75644','Habitaciones','9884','10/05/2021','85','11','3243','9884','10','Secamanos','Habitaciones','24','159');

Select * from dbo.Producto Proveedores

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75635','1');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75636','2');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75637','3');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75638','4');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75639','5');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75640','6');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75641','7');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75642','8');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75643','9');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Producto Proveedores]([CodigoProducto],[IdProveedor])VALUES('75644','10');

Select * from dbo.Proveedores

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('1','0230','Martha Acevedo','Primero de Mayo','Managua','Nicaragua','4616','22679644','marthalorena_acevedo@hotmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('2','0231','Freddy Cruz','Villa Venezuela ','Matagalpa','Nicaragua','4617','22005276','freddy_cruz@outlook.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('3','0232','Jorge Lopez','Altagracia','Granada','Nicaragua','4618','28753753','jorge-lopez@hotmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('4','0233','Luis Dominguez','Primero de Mayo','Managua','Nicaragua','4619','23326533','luis2375@yahoo.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('5','0234','Jose Castellon','Armando Guido','Juigalpa','Nicaragua','4620','20376537','josecastellon@gmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('6','0235','Patricia Escobar','Carretera Norte','Boaco','Nicaragua','4621','27635786','paty_escobar@yahoo.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('7','0236','Ricardo Hernandez','Montoya','Esteli','Nicaragua','4622','28754835','hernandez_ricardo@yahoo.es');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('8','0237','Laura Flores','Primero de Mayo','Managua','Nicaragua','4623','23655372','flores_laura@hotmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('9','0238','Monica Torrez','Dorado','Masaya','Nicaragua','4624','23655433','mony_tatty@yahoo.es');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('10','0239','Antonio Sanchez','Altagracia','Granada','Nicaragua','4625','27654545','antonio_s@gmail.com');

Select * from [dbo].[Tipo de Movimiento]

INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('1','0230','Martha Acevedo','Primero de Mayo','Managua','Nicaragua','4616','22679644','marthalorena_acevedo@hotmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('2','0231','Freddy Cruz','Villa Venezuela ','Matagalpa','Nicaragua','4617','22005276','freddy_cruz@outlook.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('3','0232','Jorge Lopez','Altagracia','Granada','Nicaragua','4618','28753753','jorge-lopez@hotmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('4','0233','Luis Dominguez','Primero de Mayo','Managua','Nicaragua','4619','23326533','luis2375@yahoo.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('5','0234','Jose Castellon','Armando Guido','Juigalpa','Nicaragua','4620','20376537','josecastellon@gmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('6','0235','Patricia Escobar','Carretera Norte','Boaco','Nicaragua','4621','27635786','paty_escobar@yahoo.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('7','0236','Ricardo Hernandez','Montoya','Esteli','Nicaragua','4622','28754835','hernandez_ricardo@yahoo.es');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('8','0237','Laura Flores','Primero de Mayo','Managua','Nicaragua','4623','23655372','flores_laura@hotmail.com');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('9','0238','Monica Torrez','Dorado','Masaya','Nicaragua','4624','23655433','mony_tatty@yahoo.es');
INSERT INTO [Control de Inventario y Proveedores].[dbo].[Proveedores]([IdProveedor],[Descripcion],[Nombre],[Direccion de la Empresa],[Departamento],[Pais],[CodigoPostal],[Telefono],[Email])VALUES('10','0239','Antonio Sanchez','Altagracia','Granada','Nicaragua','4625','27654545','antonio_s@gmail.com');


