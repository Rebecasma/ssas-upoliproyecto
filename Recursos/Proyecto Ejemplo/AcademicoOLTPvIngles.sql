/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      Universidad Politecnica de Nicaragua
 * Project :      AcademicoOLTP.dm1
 * Author :       Ing. Eduardo Tra�a
 *
 * Date Created : Saturday, May 18, 2013 15:45:53
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE Academico2
go
USE Academico2
go
IF OBJECT_ID('Career') IS NOT NULL
BEGIN
    DROP TABLE Career
    PRINT '<<< DROPPED TABLE Career >>>'
END
go
IF OBJECT_ID('Employee') IS NOT NULL
BEGIN
    DROP TABLE Employee
    PRINT '<<< DROPPED TABLE Employee >>>'
END
go
IF OBJECT_ID('Faculty') IS NOT NULL
BEGIN
    DROP TABLE Faculty
    PRINT '<<< DROPPED TABLE Faculty >>>'
END
go
IF OBJECT_ID('Grades') IS NOT NULL
BEGIN
    DROP TABLE Grades
    PRINT '<<< DROPPED TABLE Grades >>>'
END
go
IF OBJECT_ID('IdentityType') IS NOT NULL
BEGIN
    DROP TABLE IdentityType
    PRINT '<<< DROPPED TABLE IdentityType >>>'
END
go
IF OBJECT_ID('Modality') IS NOT NULL
BEGIN
    DROP TABLE Modality
    PRINT '<<< DROPPED TABLE Modality >>>'
END
go
IF OBJECT_ID('Period') IS NOT NULL
BEGIN
    DROP TABLE Period
    PRINT '<<< DROPPED TABLE Period >>>'
END
go
IF OBJECT_ID('PeriodDetail') IS NOT NULL
BEGIN
    DROP TABLE PeriodDetail
    PRINT '<<< DROPPED TABLE PeriodDetail >>>'
END
go
IF OBJECT_ID('Person') IS NOT NULL
BEGIN
    DROP TABLE Person
    PRINT '<<< DROPPED TABLE Person >>>'
END
go
IF OBJECT_ID('PlanCareer') IS NOT NULL
BEGIN
    DROP TABLE PlanCareer
    PRINT '<<< DROPPED TABLE PlanCareer >>>'
END
go
IF OBJECT_ID('Position') IS NOT NULL
BEGIN
    DROP TABLE Position
    PRINT '<<< DROPPED TABLE Position >>>'
END
go
IF OBJECT_ID('Schedule') IS NOT NULL
BEGIN
    DROP TABLE Schedule
    PRINT '<<< DROPPED TABLE Schedule >>>'
END
go
IF OBJECT_ID('Student') IS NOT NULL
BEGIN
    DROP TABLE Student
    PRINT '<<< DROPPED TABLE Student >>>'
END
go
IF OBJECT_ID('StudentCareer') IS NOT NULL
BEGIN
    DROP TABLE StudentCareer
    PRINT '<<< DROPPED TABLE StudentCareer >>>'
END
go
IF OBJECT_ID('StudentGroup') IS NOT NULL
BEGIN
    DROP TABLE StudentGroup
    PRINT '<<< DROPPED TABLE StudentGroup >>>'
END
go
IF OBJECT_ID('Subject') IS NOT NULL
BEGIN
    DROP TABLE Subject
    PRINT '<<< DROPPED TABLE Subject >>>'
END
go
IF OBJECT_ID('Teacher') IS NOT NULL
BEGIN
    DROP TABLE Teacher
    PRINT '<<< DROPPED TABLE Teacher >>>'
END
go
/* 
 * TABLE: Career 
 */

CREATE TABLE Career(
    CareerID        int              IDENTITY(1,1),
    Name            nvarchar(100)    NOT NULL,
    Abbreviation    nvarchar(6)      NOT NULL,
    DateCreation    date             NOT NULL,
    FacultyID       int              NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY CLUSTERED (CareerID)
)
go



IF OBJECT_ID('Career') IS NOT NULL
    PRINT '<<< CREATED TABLE Career >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Career >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Career', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Career'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Carreras', 'schema', 'dbo', 'table', 'Career'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'CareerID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'CareerID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'Career', 'column', 'CareerID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'Name'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'Name'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre Carrera', 'schema', 'dbo', 'table', 'Career', 'column', 'Name'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'Abbreviation'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'Abbreviation'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Abreviatura', 'schema', 'dbo', 'table', 'Career', 'column', 'Abbreviation'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'DateCreation'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'DateCreation'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Fecha Creaci�n', 'schema', 'dbo', 'table', 'Career', 'column', 'DateCreation'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'FacultyID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Career', 'column', 'FacultyID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'Career', 'column', 'FacultyID'
go
/* 
 * TABLE: Employee 
 */

CREATE TABLE Employee(
    EmployeeID         int     IDENTITY(1,1),
    PersonID           int     NOT NULL,
    PositionID         int     NOT NULL,
    InscriptionDate    date    NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY CLUSTERED (EmployeeID)
)
go



IF OBJECT_ID('Employee') IS NOT NULL
    PRINT '<<< CREATED TABLE Employee >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Employee >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Employee', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Employee'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Empleados', 'schema', 'dbo', 'table', 'Employee'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Employee', 'column', 'PersonID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Employee', 'column', 'PersonID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Employee', 'column', 'PersonID'
go
/* 
 * TABLE: Faculty 
 */

CREATE TABLE Faculty(
    FacultyID       int              IDENTITY(1,1),
    Name            nvarchar(100)    NOT NULL,
    Abbreviation    nvarchar(6)      NULL,
    DateCreation    date             NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY CLUSTERED (FacultyID)
)
go



IF OBJECT_ID('Faculty') IS NOT NULL
    PRINT '<<< CREATED TABLE Faculty >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Faculty >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Faculty', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Faculty'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Facultades', 'schema', 'dbo', 'table', 'Faculty'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Faculty', 'column', 'FacultyID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Faculty', 'column', 'FacultyID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'Faculty', 'column', 'FacultyID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Faculty', 'column', 'Name'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Faculty', 'column', 'Name'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre o Descripci�n de Facultad', 'schema', 'dbo', 'table', 'Faculty', 'column', 'Name'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Faculty', 'column', 'Abbreviation'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Faculty', 'column', 'Abbreviation'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Abreviatura', 'schema', 'dbo', 'table', 'Faculty', 'column', 'Abbreviation'
go
/* 
 * TABLE: Grades 
 */

CREATE TABLE Grades(
    StudentGroupID     int               NOT NULL,
    StudentCareerID    int               NOT NULL,
    FinalGrade         numeric(10, 2)    DEFAULT 0 NOT NULL
                       CONSTRAINT CK_MaxGrade CHECK (FinalGrade<101),
    Status             char(1)           NULL
                       CONSTRAINT CK_GradeStatus CHECK (Status='I' OR Status='M'  OR Status='N' ),
    CONSTRAINT PK17 PRIMARY KEY CLUSTERED (StudentGroupID, StudentCareerID)
)
go



IF OBJECT_ID('Grades') IS NOT NULL
    PRINT '<<< CREATED TABLE Grades >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Grades >>>'
go

/* 
 * TABLE: IdentityType 
 */

CREATE TABLE IdentityType(
    IdentityTypeID    int             IDENTITY(1,1),
    Description       nvarchar(30)    NOT NULL,
    CONSTRAINT PK12 PRIMARY KEY CLUSTERED (IdentityTypeID)
)
go



IF OBJECT_ID('IdentityType') IS NOT NULL
    PRINT '<<< CREATED TABLE IdentityType >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE IdentityType >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'IdentityType', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'IdentityType'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tipo de Identidades', 'schema', 'dbo', 'table', 'IdentityType'
go
/* 
 * TABLE: Modality 
 */

CREATE TABLE Modality(
    ModalityID     int             IDENTITY(1,1),
    Description    nvarchar(50)    NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY CLUSTERED (ModalityID)
)
go



IF OBJECT_ID('Modality') IS NOT NULL
    PRINT '<<< CREATED TABLE Modality >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Modality >>>'
go

/* 
 * TABLE: Period 
 */

CREATE TABLE Period(
    PeriodID     int        IDENTITY(1,1),
    PerID        char(4)    NOT NULL,
    BeginDate    date       NULL,
    EndDate      date       NULL,
    Status       char(1)    NOT NULL
                 CONSTRAINT CK_Estatus CHECK (Status='A' OR Status='B' OR Status='C'),
    CONSTRAINT PK14 PRIMARY KEY CLUSTERED (PeriodID)
)
go



IF OBJECT_ID('Period') IS NOT NULL
    PRINT '<<< CREATED TABLE Period >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Period >>>'
go

/* 
 * TABLE: PeriodDetail 
 */

CREATE TABLE PeriodDetail(
    PeriodDetailID    int        IDENTITY(1,1),
    PerDetID          char(6)    NOT NULL,
    BeginDate         date       NOT NULL,
    EndDate           date       NOT NULL,
    Orden             tinyint    NOT NULL,
    PeriodID          int        NOT NULL,
    CONSTRAINT PK15 PRIMARY KEY CLUSTERED (PeriodDetailID)
)
go



IF OBJECT_ID('PeriodDetail') IS NOT NULL
    PRINT '<<< CREATED TABLE PeriodDetail >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE PeriodDetail >>>'
go

/* 
 * TABLE: Person 
 */

CREATE TABLE Person(
    PersonID          int             IDENTITY(1,1),
    Name              nvarchar(70)    NOT NULL,
    LastName          nvarchar(70)    NOT NULL,
    Sex               nchar(1)        NOT NULL
                      CONSTRAINT CK_Sexo CHECK (Sex='F' or  Sex='M' ),
    IdentityTypeID    int             NOT NULL,
    IdentityValue     nvarchar(25)    NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY CLUSTERED (PersonID)
)
go



IF OBJECT_ID('Person') IS NOT NULL
    PRINT '<<< CREATED TABLE Person >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Person >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Person', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Person'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Personas', 'schema', 'dbo', 'table', 'Person'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Person', 'column', 'PersonID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Person', 'column', 'PersonID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Person', 'column', 'PersonID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Person', 'column', 'IdentityValue'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Person', 'column', 'IdentityValue'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Identidad Asociada', 'schema', 'dbo', 'table', 'Person', 'column', 'IdentityValue'
go
/* 
 * TABLE: PlanCareer 
 */

CREATE TABLE PlanCareer(
    PlanCareerID               int        IDENTITY(1,1),
    CareerID                   int        NOT NULL,
    SubjectID                  int        NOT NULL,
    PrerrequisitoAsignatura    int        NOT NULL,
    Credits                    int        NOT NULL,
    Status                     char(1)    NULL
                               CONSTRAINT CK_EstatusPlan CHECK (Status='A' OR Status='C'),
    ModalityID                 int        NOT NULL,
    NumbersHours               int        NULL,
    CONSTRAINT PK13 PRIMARY KEY CLUSTERED (PlanCareerID)
)
go



IF OBJECT_ID('PlanCareer') IS NOT NULL
    PRINT '<<< CREATED TABLE PlanCareer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE PlanCareer >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'CareerID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'CareerID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'CareerID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'SubjectID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'SubjectID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'SubjectID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'PrerrequisitoAsignatura'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'PrerrequisitoAsignatura'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'PlanCareer', 'column', 'PrerrequisitoAsignatura'
go
/* 
 * TABLE: Position 
 */

CREATE TABLE Position(
    PositionID     int              IDENTITY(1,1),
    Description    nvarchar(100)    NULL,
    CONSTRAINT PK9 PRIMARY KEY CLUSTERED (PositionID)
)
go



IF OBJECT_ID('Position') IS NOT NULL
    PRINT '<<< CREATED TABLE Position >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Position >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Position', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Position'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Cargos Academicos', 'schema', 'dbo', 'table', 'Position'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Position', 'column', 'Description'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Position', 'column', 'Description'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre del Cargo', 'schema', 'dbo', 'table', 'Position', 'column', 'Description'
go
/* 
 * TABLE: Schedule 
 */

CREATE TABLE Schedule(
    ScheduleID       int             IDENTITY(1,1),
    Day              nvarchar(12)    NOT NULL,
    EntryHour        time(7)         NOT NULL,
    DepartureHour    time(7)         NULL,
    CONSTRAINT PK7 PRIMARY KEY CLUSTERED (ScheduleID)
)
go



IF OBJECT_ID('Schedule') IS NOT NULL
    PRINT '<<< CREATED TABLE Schedule >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Schedule >>>'
go

/* 
 * TABLE: Student 
 */

CREATE TABLE Student(
    StudentID    int    IDENTITY(1,1),
    PersonID     int    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY CLUSTERED (StudentID)
)
go



IF OBJECT_ID('Student') IS NOT NULL
    PRINT '<<< CREATED TABLE Student >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Student >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Student', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Student'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Estudiantes', 'schema', 'dbo', 'table', 'Student'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Student', 'column', 'PersonID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Student', 'column', 'PersonID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Student', 'column', 'PersonID'
go
/* 
 * TABLE: StudentCareer 
 */

CREATE TABLE StudentCareer(
    StudentCareerID    int     IDENTITY(1,1),
    StudentID          int     NOT NULL,
    FacultyID          int     NOT NULL,
    CareerID           int     NOT NULL,
    PlanCareerID       int     NOT NULL,
    InscriptionDate    date    NOT NULL,
    RetirementDate     date    NULL,
    CulminationDate    date    NULL,
    CONSTRAINT PK16 PRIMARY KEY CLUSTERED (StudentCareerID)
)
go



IF OBJECT_ID('StudentCareer') IS NOT NULL
    PRINT '<<< CREATED TABLE StudentCareer >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE StudentCareer >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'StudentCareer', 'column', 'FacultyID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'StudentCareer', 'column', 'FacultyID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'StudentCareer', 'column', 'FacultyID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'StudentCareer', 'column', 'CareerID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'StudentCareer', 'column', 'CareerID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'StudentCareer', 'column', 'CareerID'
go
/* 
 * TABLE: StudentGroup 
 */

CREATE TABLE StudentGroup(
    StudentGroupID      int         IDENTITY(1,1),
    StudentGroupCode    char(10)    NOT NULL,
    PeriodID            int         NOT NULL,
    PeriodDetailID      int         NOT NULL,
    FacultyID           int         NOT NULL,
    CareerID            int         NOT NULL,
    PlanCareerID        int         NOT NULL,
    SubjectID           int         NOT NULL,
    ScheduleID          int         NOT NULL,
    TeacherID           int         NOT NULL,
    MinimumQuota        int         NOT NULL,
    MaximumQuota        int         NOT NULL,
    RealQuota           int         NOT NULL,
    Status              char(1)     NULL,
    CONSTRAINT PK10 PRIMARY KEY CLUSTERED (StudentGroupID)
)
go



IF OBJECT_ID('StudentGroup') IS NOT NULL
    PRINT '<<< CREATED TABLE StudentGroup >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE StudentGroup >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'FacultyID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'FacultyID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'FacultyID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'CareerID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'CareerID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'CareerID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'SubjectID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'SubjectID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'StudentGroup', 'column', 'SubjectID'
go
/* 
 * TABLE: Subject 
 */

CREATE TABLE Subject(
    SubjectID    int              IDENTITY(1,1),
    Subject      nvarchar(100)    NOT NULL,
    CONSTRAINT PK5 PRIMARY KEY CLUSTERED (SubjectID)
)
go



IF OBJECT_ID('Subject') IS NOT NULL
    PRINT '<<< CREATED TABLE Subject >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Subject >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Subject', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Subject'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Asignaturas', 'schema', 'dbo', 'table', 'Subject'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Subject', 'column', 'SubjectID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Subject', 'column', 'SubjectID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'Subject', 'column', 'SubjectID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Subject', 'column', 'Subject'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Subject', 'column', 'Subject'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre de Asignatura', 'schema', 'dbo', 'table', 'Subject', 'column', 'Subject'
go
/* 
 * TABLE: Teacher 
 */

CREATE TABLE Teacher(
    TeacherID    int    IDENTITY(1,1),
    PersonID     int    NOT NULL,
    CONSTRAINT PK4 PRIMARY KEY CLUSTERED (TeacherID)
)
go



IF OBJECT_ID('Teacher') IS NOT NULL
    PRINT '<<< CREATED TABLE Teacher >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Teacher >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Teacher', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Teacher'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Docentes', 'schema', 'dbo', 'table', 'Teacher'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Teacher', 'column', 'PersonID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Teacher', 'column', 'PersonID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Teacher', 'column', 'PersonID'
go
/* 
 * INDEX: Ref11 
 */

CREATE INDEX Ref11 ON Career(FacultyID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Career') AND name='Ref11')
    PRINT '<<< CREATED INDEX Career.Ref11 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Career.Ref11 >>>'
go

/* 
 * INDEX: Ref112 
 */

CREATE INDEX Ref112 ON Employee(PersonID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Employee') AND name='Ref112')
    PRINT '<<< CREATED INDEX Employee.Ref112 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Employee.Ref112 >>>'
go

/* 
 * INDEX: Ref95 
 */

CREATE INDEX Ref95 ON Employee(PositionID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Employee') AND name='Ref95')
    PRINT '<<< CREATED INDEX Employee.Ref95 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Employee.Ref95 >>>'
go

/* 
 * INDEX: Ref1023 
 */

CREATE INDEX Ref1023 ON Grades(StudentGroupID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Grades') AND name='Ref1023')
    PRINT '<<< CREATED INDEX Grades.Ref1023 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Grades.Ref1023 >>>'
go

/* 
 * INDEX: Ref1624 
 */

CREATE INDEX Ref1624 ON Grades(StudentCareerID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Grades') AND name='Ref1624')
    PRINT '<<< CREATED INDEX Grades.Ref1624 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Grades.Ref1624 >>>'
go

/* 
 * INDEX: Ref149 
 */

CREATE INDEX Ref149 ON PeriodDetail(PeriodID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('PeriodDetail') AND name='Ref149')
    PRINT '<<< CREATED INDEX PeriodDetail.Ref149 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX PeriodDetail.Ref149 >>>'
go

/* 
 * INDEX: Ref126 
 */

CREATE INDEX Ref126 ON Person(IdentityTypeID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Person') AND name='Ref126')
    PRINT '<<< CREATED INDEX Person.Ref126 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Person.Ref126 >>>'
go

/* 
 * INDEX: Ref27 
 */

CREATE INDEX Ref27 ON PlanCareer(CareerID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('PlanCareer') AND name='Ref27')
    PRINT '<<< CREATED INDEX PlanCareer.Ref27 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX PlanCareer.Ref27 >>>'
go

/* 
 * INDEX: Ref58 
 */

CREATE INDEX Ref58 ON PlanCareer(SubjectID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('PlanCareer') AND name='Ref58')
    PRINT '<<< CREATED INDEX PlanCareer.Ref58 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX PlanCareer.Ref58 >>>'
go

/* 
 * INDEX: Ref610 
 */

CREATE INDEX Ref610 ON PlanCareer(ModalityID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('PlanCareer') AND name='Ref610')
    PRINT '<<< CREATED INDEX PlanCareer.Ref610 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX PlanCareer.Ref610 >>>'
go

/* 
 * INDEX: Ref525 
 */

CREATE INDEX Ref525 ON PlanCareer(PrerrequisitoAsignatura)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('PlanCareer') AND name='Ref525')
    PRINT '<<< CREATED INDEX PlanCareer.Ref525 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX PlanCareer.Ref525 >>>'
go

/* 
 * INDEX: Ref113 
 */

CREATE INDEX Ref113 ON Student(PersonID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Student') AND name='Ref113')
    PRINT '<<< CREATED INDEX Student.Ref113 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Student.Ref113 >>>'
go

/* 
 * INDEX: Ref319 
 */

CREATE INDEX Ref319 ON StudentCareer(StudentID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentCareer') AND name='Ref319')
    PRINT '<<< CREATED INDEX StudentCareer.Ref319 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentCareer.Ref319 >>>'
go

/* 
 * INDEX: Ref220 
 */

CREATE INDEX Ref220 ON StudentCareer(CareerID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentCareer') AND name='Ref220')
    PRINT '<<< CREATED INDEX StudentCareer.Ref220 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentCareer.Ref220 >>>'
go

/* 
 * INDEX: Ref1321 
 */

CREATE INDEX Ref1321 ON StudentCareer(PlanCareerID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentCareer') AND name='Ref1321')
    PRINT '<<< CREATED INDEX StudentCareer.Ref1321 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentCareer.Ref1321 >>>'
go

/* 
 * INDEX: Ref122 
 */

CREATE INDEX Ref122 ON StudentCareer(FacultyID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentCareer') AND name='Ref122')
    PRINT '<<< CREATED INDEX StudentCareer.Ref122 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentCareer.Ref122 >>>'
go

/* 
 * INDEX: Ref1511 
 */

CREATE INDEX Ref1511 ON StudentGroup(PeriodDetailID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref1511')
    PRINT '<<< CREATED INDEX StudentGroup.Ref1511 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref1511 >>>'
go

/* 
 * INDEX: Ref1412 
 */

CREATE INDEX Ref1412 ON StudentGroup(PeriodID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref1412')
    PRINT '<<< CREATED INDEX StudentGroup.Ref1412 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref1412 >>>'
go

/* 
 * INDEX: Ref1313 
 */

CREATE INDEX Ref1313 ON StudentGroup(PlanCareerID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref1313')
    PRINT '<<< CREATED INDEX StudentGroup.Ref1313 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref1313 >>>'
go

/* 
 * INDEX: Ref214 
 */

CREATE INDEX Ref214 ON StudentGroup(CareerID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref214')
    PRINT '<<< CREATED INDEX StudentGroup.Ref214 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref214 >>>'
go

/* 
 * INDEX: Ref115 
 */

CREATE INDEX Ref115 ON StudentGroup(FacultyID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref115')
    PRINT '<<< CREATED INDEX StudentGroup.Ref115 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref115 >>>'
go

/* 
 * INDEX: Ref516 
 */

CREATE INDEX Ref516 ON StudentGroup(SubjectID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref516')
    PRINT '<<< CREATED INDEX StudentGroup.Ref516 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref516 >>>'
go

/* 
 * INDEX: Ref717 
 */

CREATE INDEX Ref717 ON StudentGroup(ScheduleID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref717')
    PRINT '<<< CREATED INDEX StudentGroup.Ref717 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref717 >>>'
go

/* 
 * INDEX: Ref418 
 */

CREATE INDEX Ref418 ON StudentGroup(TeacherID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('StudentGroup') AND name='Ref418')
    PRINT '<<< CREATED INDEX StudentGroup.Ref418 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX StudentGroup.Ref418 >>>'
go

/* 
 * INDEX: Ref114 
 */

CREATE INDEX Ref114 ON Teacher(PersonID)
go
IF EXISTS (SELECT * FROM sys.indexes WHERE object_id=OBJECT_ID('Teacher') AND name='Ref114')
    PRINT '<<< CREATED INDEX Teacher.Ref114 >>>'
ELSE
    PRINT '<<< FAILED CREATING INDEX Teacher.Ref114 >>>'
go

/* 
 * TABLE: Career 
 */

ALTER TABLE Career ADD CONSTRAINT RefFaculty1 
    FOREIGN KEY (FacultyID)
    REFERENCES Faculty(FacultyID)
go


/* 
 * TABLE: Employee 
 */

ALTER TABLE Employee ADD CONSTRAINT RefPerson2 
    FOREIGN KEY (PersonID)
    REFERENCES Person(PersonID)
go

ALTER TABLE Employee ADD CONSTRAINT RefPosition5 
    FOREIGN KEY (PositionID)
    REFERENCES Position(PositionID)
go


/* 
 * TABLE: Grades 
 */

ALTER TABLE Grades ADD CONSTRAINT RefStudentGroup23 
    FOREIGN KEY (StudentGroupID)
    REFERENCES StudentGroup(StudentGroupID)
go

ALTER TABLE Grades ADD CONSTRAINT RefStudentCareer24 
    FOREIGN KEY (StudentCareerID)
    REFERENCES StudentCareer(StudentCareerID)
go


/* 
 * TABLE: PeriodDetail 
 */

ALTER TABLE PeriodDetail ADD CONSTRAINT RefPeriod9 
    FOREIGN KEY (PeriodID)
    REFERENCES Period(PeriodID)
go


/* 
 * TABLE: Person 
 */

ALTER TABLE Person ADD CONSTRAINT RefIdentityType6 
    FOREIGN KEY (IdentityTypeID)
    REFERENCES IdentityType(IdentityTypeID)
go


/* 
 * TABLE: PlanCareer 
 */

ALTER TABLE PlanCareer ADD CONSTRAINT RefCareer7 
    FOREIGN KEY (CareerID)
    REFERENCES Career(CareerID)
go

ALTER TABLE PlanCareer ADD CONSTRAINT RefSubject8 
    FOREIGN KEY (SubjectID)
    REFERENCES Subject(SubjectID)
go

ALTER TABLE PlanCareer ADD CONSTRAINT RefModality10 
    FOREIGN KEY (ModalityID)
    REFERENCES Modality(ModalityID)
go

ALTER TABLE PlanCareer ADD CONSTRAINT RefSubject25 
    FOREIGN KEY (PrerrequisitoAsignatura)
    REFERENCES Subject(SubjectID)
go


/* 
 * TABLE: Student 
 */

ALTER TABLE Student ADD CONSTRAINT RefPerson3 
    FOREIGN KEY (PersonID)
    REFERENCES Person(PersonID)
go


/* 
 * TABLE: StudentCareer 
 */

ALTER TABLE StudentCareer ADD CONSTRAINT RefStudent19 
    FOREIGN KEY (StudentID)
    REFERENCES Student(StudentID)
go

ALTER TABLE StudentCareer ADD CONSTRAINT RefCareer20 
    FOREIGN KEY (CareerID)
    REFERENCES Career(CareerID)
go

ALTER TABLE StudentCareer ADD CONSTRAINT RefPlanCareer21 
    FOREIGN KEY (PlanCareerID)
    REFERENCES PlanCareer(PlanCareerID)
go

ALTER TABLE StudentCareer ADD CONSTRAINT RefFaculty22 
    FOREIGN KEY (FacultyID)
    REFERENCES Faculty(FacultyID)
go


/* 
 * TABLE: StudentGroup 
 */

ALTER TABLE StudentGroup ADD CONSTRAINT RefPeriodDetail11 
    FOREIGN KEY (PeriodDetailID)
    REFERENCES PeriodDetail(PeriodDetailID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefPeriod12 
    FOREIGN KEY (PeriodID)
    REFERENCES Period(PeriodID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefPlanCareer13 
    FOREIGN KEY (PlanCareerID)
    REFERENCES PlanCareer(PlanCareerID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefCareer14 
    FOREIGN KEY (CareerID)
    REFERENCES Career(CareerID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefFaculty15 
    FOREIGN KEY (FacultyID)
    REFERENCES Faculty(FacultyID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefSubject16 
    FOREIGN KEY (SubjectID)
    REFERENCES Subject(SubjectID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefSchedule17 
    FOREIGN KEY (ScheduleID)
    REFERENCES Schedule(ScheduleID)
go

ALTER TABLE StudentGroup ADD CONSTRAINT RefTeacher18 
    FOREIGN KEY (TeacherID)
    REFERENCES Teacher(TeacherID)
go


/* 
 * TABLE: Teacher 
 */

ALTER TABLE Teacher ADD CONSTRAINT RefPerson4 
    FOREIGN KEY (PersonID)
    REFERENCES Person(PersonID)
go


