/*
 * ER/Studio 8.0 SQL Code Generation
 * Company :      Universidad Politecnica de Nicaragua
 * Project :      AcademicoOLTP.dm1
 * Author :       Ing. Eduardo Tra�a
 *
 * Date Created : Saturday, May 18, 2013 14:22:40
 * Target DBMS : Microsoft SQL Server 2008
 */

USE master
go
CREATE DATABASE Academico
go
USE Academico
go
IF OBJECT_ID('Asignatura') IS NOT NULL
BEGIN
    DROP TABLE Asignatura
    PRINT '<<< DROPPED TABLE Asignatura >>>'
END
go
IF OBJECT_ID('Cargo') IS NOT NULL
BEGIN
    DROP TABLE Cargo
    PRINT '<<< DROPPED TABLE Cargo >>>'
END
go
IF OBJECT_ID('Carrera') IS NOT NULL
BEGIN
    DROP TABLE Carrera
    PRINT '<<< DROPPED TABLE Carrera >>>'
END
go
IF OBJECT_ID('Docente') IS NOT NULL
BEGIN
    DROP TABLE Docente
    PRINT '<<< DROPPED TABLE Docente >>>'
END
go
IF OBJECT_ID('Empleado') IS NOT NULL
BEGIN
    DROP TABLE Empleado
    PRINT '<<< DROPPED TABLE Empleado >>>'
END
go
IF OBJECT_ID('Estudiante') IS NOT NULL
BEGIN
    DROP TABLE Estudiante
    PRINT '<<< DROPPED TABLE Estudiante >>>'
END
go
IF OBJECT_ID('EstudianteCarrera') IS NOT NULL
BEGIN
    DROP TABLE EstudianteCarrera
    PRINT '<<< DROPPED TABLE EstudianteCarrera >>>'
END
go
IF OBJECT_ID('Facultad') IS NOT NULL
BEGIN
    DROP TABLE Facultad
    PRINT '<<< DROPPED TABLE Facultad >>>'
END
go
IF OBJECT_ID('Grupo') IS NOT NULL
BEGIN
    DROP TABLE Grupo
    PRINT '<<< DROPPED TABLE Grupo >>>'
END
go
IF OBJECT_ID('Horario') IS NOT NULL
BEGIN
    DROP TABLE Horario
    PRINT '<<< DROPPED TABLE Horario >>>'
END
go
IF OBJECT_ID('Modalidad') IS NOT NULL
BEGIN
    DROP TABLE Modalidad
    PRINT '<<< DROPPED TABLE Modalidad >>>'
END
go
IF OBJECT_ID('Notas') IS NOT NULL
BEGIN
    DROP TABLE Notas
    PRINT '<<< DROPPED TABLE Notas >>>'
END
go
IF OBJECT_ID('Periodo') IS NOT NULL
BEGIN
    DROP TABLE Periodo
    PRINT '<<< DROPPED TABLE Periodo >>>'
END
go
IF OBJECT_ID('PeriodoDetalle') IS NOT NULL
BEGIN
    DROP TABLE PeriodoDetalle
    PRINT '<<< DROPPED TABLE PeriodoDetalle >>>'
END
go
IF OBJECT_ID('Persona') IS NOT NULL
BEGIN
    DROP TABLE Persona
    PRINT '<<< DROPPED TABLE Persona >>>'
END
go
IF OBJECT_ID('PlanCarrera') IS NOT NULL
BEGIN
    DROP TABLE PlanCarrera
    PRINT '<<< DROPPED TABLE PlanCarrera >>>'
END
go
IF OBJECT_ID('TipoIdentidad') IS NOT NULL
BEGIN
    DROP TABLE TipoIdentidad
    PRINT '<<< DROPPED TABLE TipoIdentidad >>>'
END
go
/* 
 * TABLE: Asignatura 
 */

CREATE TABLE Asignatura(
    AsignaturaID        int              IDENTITY(1,1),
    NombreAsignatura    nvarchar(100)    NOT NULL,
    CONSTRAINT PK5 PRIMARY KEY CLUSTERED (AsignaturaID)
)
go



IF OBJECT_ID('Asignatura') IS NOT NULL
    PRINT '<<< CREATED TABLE Asignatura >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Asignatura >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Asignatura', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Asignatura'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Asignaturas', 'schema', 'dbo', 'table', 'Asignatura'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Asignatura', 'column', 'AsignaturaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Asignatura', 'column', 'AsignaturaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'Asignatura', 'column', 'AsignaturaID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Asignatura', 'column', 'NombreAsignatura'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Asignatura', 'column', 'NombreAsignatura'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre de Asignatura', 'schema', 'dbo', 'table', 'Asignatura', 'column', 'NombreAsignatura'
go
/* 
 * TABLE: Cargo 
 */

CREATE TABLE Cargo(
    CargoID        int              IDENTITY(1,1),
    NombreCargo    nvarchar(100)    NULL,
    CONSTRAINT PK9 PRIMARY KEY CLUSTERED (CargoID)
)
go



IF OBJECT_ID('Cargo') IS NOT NULL
    PRINT '<<< CREATED TABLE Cargo >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Cargo >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Cargo', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Cargo'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Cargos Academicos', 'schema', 'dbo', 'table', 'Cargo'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Cargo', 'column', 'NombreCargo'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Cargo', 'column', 'NombreCargo'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre del Cargo', 'schema', 'dbo', 'table', 'Cargo', 'column', 'NombreCargo'
go
/* 
 * TABLE: Carrera 
 */

CREATE TABLE Carrera(
    CarreraID        int              IDENTITY(1,1),
    NombreCarrera    nvarchar(100)    NOT NULL,
    CarreraAbrv      nvarchar(6)      NOT NULL,
    Fecha            date             NOT NULL,
    FacultadID       int              NOT NULL,
    CONSTRAINT PK2 PRIMARY KEY CLUSTERED (CarreraID)
)
go



IF OBJECT_ID('Carrera') IS NOT NULL
    PRINT '<<< CREATED TABLE Carrera >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Carrera >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Carrera', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Carrera'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Carreras', 'schema', 'dbo', 'table', 'Carrera'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'CarreraID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'CarreraID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'Carrera', 'column', 'CarreraID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'NombreCarrera'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'NombreCarrera'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre Carrera', 'schema', 'dbo', 'table', 'Carrera', 'column', 'NombreCarrera'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'CarreraAbrv'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'CarreraAbrv'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Abreviatura', 'schema', 'dbo', 'table', 'Carrera', 'column', 'CarreraAbrv'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'Fecha'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'Fecha'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Fecha Creaci�n', 'schema', 'dbo', 'table', 'Carrera', 'column', 'Fecha'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'FacultadID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Carrera', 'column', 'FacultadID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'Carrera', 'column', 'FacultadID'
go
/* 
 * TABLE: Docente 
 */

CREATE TABLE Docente(
    DocenteID    int    IDENTITY(1,1),
    PersonaID    int    NOT NULL,
    CONSTRAINT PK4 PRIMARY KEY CLUSTERED (DocenteID)
)
go



IF OBJECT_ID('Docente') IS NOT NULL
    PRINT '<<< CREATED TABLE Docente >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Docente >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Docente', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Docente'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Docentes', 'schema', 'dbo', 'table', 'Docente'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Docente', 'column', 'PersonaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Docente', 'column', 'PersonaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Docente', 'column', 'PersonaID'
go
/* 
 * TABLE: Empleado 
 */

CREATE TABLE Empleado(
    EmpleadoID      int     IDENTITY(1,1),
    PersonaID       int     NOT NULL,
    CargoID         int     NOT NULL,
    FechaIngreso    date    NOT NULL,
    CONSTRAINT PK8 PRIMARY KEY CLUSTERED (EmpleadoID)
)
go



IF OBJECT_ID('Empleado') IS NOT NULL
    PRINT '<<< CREATED TABLE Empleado >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Empleado >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Empleado', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Empleado'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Empleados', 'schema', 'dbo', 'table', 'Empleado'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Empleado', 'column', 'PersonaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Empleado', 'column', 'PersonaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Empleado', 'column', 'PersonaID'
go
/* 
 * TABLE: Estudiante 
 */

CREATE TABLE Estudiante(
    EstudianteID    int    IDENTITY(1,1),
    PersonaID       int    NOT NULL,
    CONSTRAINT PK3 PRIMARY KEY CLUSTERED (EstudianteID)
)
go



IF OBJECT_ID('Estudiante') IS NOT NULL
    PRINT '<<< CREATED TABLE Estudiante >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Estudiante >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Estudiante', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Estudiante'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Estudiantes', 'schema', 'dbo', 'table', 'Estudiante'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Estudiante', 'column', 'PersonaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Estudiante', 'column', 'PersonaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Estudiante', 'column', 'PersonaID'
go
/* 
 * TABLE: EstudianteCarrera 
 */

CREATE TABLE EstudianteCarrera(
    EstudianteCarreraID    int     IDENTITY(1,1),
    EstudianteID           int     NOT NULL,
    FacultadID             int     NOT NULL,
    CarreraID              int     NOT NULL,
    PlanCarreraID          int     NOT NULL,
    FechaInscripcion       date    NOT NULL,
    FechaRetiro            date    NULL,
    FechaCulminacion       date    NULL,
    CONSTRAINT PK16 PRIMARY KEY CLUSTERED (EstudianteCarreraID)
)
go



IF OBJECT_ID('EstudianteCarrera') IS NOT NULL
    PRINT '<<< CREATED TABLE EstudianteCarrera >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE EstudianteCarrera >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'EstudianteCarrera', 'column', 'FacultadID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'EstudianteCarrera', 'column', 'FacultadID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'EstudianteCarrera', 'column', 'FacultadID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'EstudianteCarrera', 'column', 'CarreraID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'EstudianteCarrera', 'column', 'CarreraID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'EstudianteCarrera', 'column', 'CarreraID'
go
/* 
 * TABLE: Facultad 
 */

CREATE TABLE Facultad(
    FacultadID        int              IDENTITY(1,1),
    NombreFacultad    nvarchar(100)    NOT NULL,
    FacultadAbrv      nvarchar(6)      NULL,
    FechaFundacion    date             NOT NULL,
    CONSTRAINT PK1 PRIMARY KEY CLUSTERED (FacultadID)
)
go



IF OBJECT_ID('Facultad') IS NOT NULL
    PRINT '<<< CREATED TABLE Facultad >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Facultad >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Facultad', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Facultad'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Facultades', 'schema', 'dbo', 'table', 'Facultad'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Facultad', 'column', 'FacultadID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Facultad', 'column', 'FacultadID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'Facultad', 'column', 'FacultadID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Facultad', 'column', 'NombreFacultad'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Facultad', 'column', 'NombreFacultad'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Nombre o Descripci�n de Facultad', 'schema', 'dbo', 'table', 'Facultad', 'column', 'NombreFacultad'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Facultad', 'column', 'FacultadAbrv'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Facultad', 'column', 'FacultadAbrv'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Abreviatura', 'schema', 'dbo', 'table', 'Facultad', 'column', 'FacultadAbrv'
go
/* 
 * TABLE: Grupo 
 */

CREATE TABLE Grupo(
    GrupoID             int         IDENTITY(1,1),
    GrupoCodigo         char(10)    NOT NULL,
    PeriodoID           int         NOT NULL,
    PeriodoDetalleID    int         NOT NULL,
    FacultadID          int         NOT NULL,
    CarreraID           int         NOT NULL,
    PlanCarreraID       int         NOT NULL,
    AsignaturaID        int         NOT NULL,
    HorarioID           int         NOT NULL,
    DocenteID           int         NOT NULL,
    CupoMinimo          int         NOT NULL,
    CupoMaximo          int         NOT NULL,
    CupoReal            int         NOT NULL,
    Estatus             char(1)     NULL,
    CONSTRAINT PK10 PRIMARY KEY CLUSTERED (GrupoID)
)
go



IF OBJECT_ID('Grupo') IS NOT NULL
    PRINT '<<< CREATED TABLE Grupo >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Grupo >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Grupo', 'column', 'FacultadID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Grupo', 'column', 'FacultadID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla Facultades', 'schema', 'dbo', 'table', 'Grupo', 'column', 'FacultadID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Grupo', 'column', 'CarreraID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Grupo', 'column', 'CarreraID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'Grupo', 'column', 'CarreraID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Grupo', 'column', 'AsignaturaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Grupo', 'column', 'AsignaturaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'Grupo', 'column', 'AsignaturaID'
go
/* 
 * TABLE: Horario 
 */

CREATE TABLE Horario(
    HorarioID      int             IDENTITY(1,1),
    Dia            nvarchar(12)    NOT NULL,
    HoraEntrada    time(7)         NOT NULL,
    HoraSalida     time(7)         NULL,
    CONSTRAINT PK7 PRIMARY KEY CLUSTERED (HorarioID)
)
go



IF OBJECT_ID('Horario') IS NOT NULL
    PRINT '<<< CREATED TABLE Horario >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Horario >>>'
go

/* 
 * TABLE: Modalidad 
 */

CREATE TABLE Modalidad(
    ModalidadID    int             IDENTITY(1,1),
    Descripcion    nvarchar(50)    NOT NULL,
    CONSTRAINT PK6 PRIMARY KEY CLUSTERED (ModalidadID)
)
go



IF OBJECT_ID('Modalidad') IS NOT NULL
    PRINT '<<< CREATED TABLE Modalidad >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Modalidad >>>'
go

/* 
 * TABLE: Notas 
 */

CREATE TABLE Notas(
    GrupoID                int               NOT NULL,
    EstudianteCarreraID    int               NOT NULL,
    NotaFinal              numeric(10, 2)    DEFAULT 0 NOT NULL,
    Estatus                char(1)           NULL
                           CONSTRAINT CK_EstatusNotas CHECK (Estatus='I' OR Estatus='M'  OR Estatus='N' ),
    CONSTRAINT PK17 PRIMARY KEY CLUSTERED (GrupoID, EstudianteCarreraID)
)
go



IF OBJECT_ID('Notas') IS NOT NULL
    PRINT '<<< CREATED TABLE Notas >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Notas >>>'
go

/* 
 * TABLE: Periodo 
 */

CREATE TABLE Periodo(
    PeriodoID      int        IDENTITY(1,1),
    PerID          char(4)    NOT NULL,
    FechaInicio    date       NULL,
    FechaFin       date       NULL,
    Estatus        char(1)    NOT NULL
                   CONSTRAINT CK_Estatus CHECK (Estatus='A' OR Estatus='B' OR Estatus='C'),
    CONSTRAINT PK14 PRIMARY KEY CLUSTERED (PeriodoID)
)
go



IF OBJECT_ID('Periodo') IS NOT NULL
    PRINT '<<< CREATED TABLE Periodo >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Periodo >>>'
go

/* 
 * TABLE: PeriodoDetalle 
 */

CREATE TABLE PeriodoDetalle(
    PeriodoDetalleID    int        IDENTITY(1,1),
    PerDetID            char(6)    NOT NULL,
    FechaInicio         date       NOT NULL,
    FechaFin            date       NOT NULL,
    Orden               tinyint    NOT NULL,
    PeriodoID           int        NOT NULL,
    CONSTRAINT PK15 PRIMARY KEY CLUSTERED (PeriodoDetalleID)
)
go



IF OBJECT_ID('PeriodoDetalle') IS NOT NULL
    PRINT '<<< CREATED TABLE PeriodoDetalle >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE PeriodoDetalle >>>'
go

/* 
 * TABLE: Persona 
 */

CREATE TABLE Persona(
    PersonaID          int             IDENTITY(1,1),
    Nombres            nvarchar(70)    NOT NULL,
    Apellidos          nvarchar(70)    NOT NULL,
    Sexo               nchar(1)        NOT NULL
                       CONSTRAINT CK_Sexo CHECK (Sexo='F' or  Sexo='M' ),
    TipoIdentidadID    int             NOT NULL,
    Identidad          nvarchar(25)    NOT NULL,
    CONSTRAINT PK11 PRIMARY KEY CLUSTERED (PersonaID)
)
go



IF OBJECT_ID('Persona') IS NOT NULL
    PRINT '<<< CREATED TABLE Persona >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE Persona >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Persona', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Persona'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tabla de Personas', 'schema', 'dbo', 'table', 'Persona'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Persona', 'column', 'PersonaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Persona', 'column', 'PersonaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'ID de Persona', 'schema', 'dbo', 'table', 'Persona', 'column', 'PersonaID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'Persona', 'column', 'Identidad'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'Persona', 'column', 'Identidad'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Identidad Asociada', 'schema', 'dbo', 'table', 'Persona', 'column', 'Identidad'
go
/* 
 * TABLE: PlanCarrera 
 */

CREATE TABLE PlanCarrera(
    PlanCarreraID              int        IDENTITY(1,1),
    CarreraID                  int        NOT NULL,
    AsignaturaID               int        NOT NULL,
    PrerrequisitoAsignatura    int        NOT NULL,
    Creditos                   int        NOT NULL,
    Estatus                    char(1)    NULL
                               CONSTRAINT CK_EstatusPlan CHECK (Estatus='A' OR Estatus='C'),
    ModalidadID                int        NOT NULL,
    CantidadHoras              int        NULL,
    CONSTRAINT PK13 PRIMARY KEY CLUSTERED (PlanCarreraID)
)
go



IF OBJECT_ID('PlanCarrera') IS NOT NULL
    PRINT '<<< CREATED TABLE PlanCarrera >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE PlanCarrera >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'CarreraID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'CarreraID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Carrera', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'CarreraID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'AsignaturaID'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'AsignaturaID'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'AsignaturaID'
go
if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'PrerrequisitoAsignatura'))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'PrerrequisitoAsignatura'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Llave de Tabla de Asignaturas', 'schema', 'dbo', 'table', 'PlanCarrera', 'column', 'PrerrequisitoAsignatura'
go
/* 
 * TABLE: TipoIdentidad 
 */

CREATE TABLE TipoIdentidad(
    TipoIdentidadID    int             IDENTITY(1,1),
    Descripcion        nvarchar(30)    NOT NULL,
    CONSTRAINT PK12 PRIMARY KEY CLUSTERED (TipoIdentidadID)
)
go



IF OBJECT_ID('TipoIdentidad') IS NOT NULL
    PRINT '<<< CREATED TABLE TipoIdentidad >>>'
ELSE
    PRINT '<<< FAILED CREATING TABLE TipoIdentidad >>>'
go

if exists (select * from ::fn_listextendedproperty('MS_Description', 'schema', 'dbo', 'table', 'TipoIdentidad', default, default))
BEGIN
    exec sys.sp_dropextendedproperty 'MS_Description', 'schema', 'dbo', 'table', 'TipoIdentidad'
END
exec sys.sp_addextendedproperty 'MS_Description', 'Tipo de Identidades', 'schema', 'dbo', 'table', 'TipoIdentidad'
go
/* 
 * TABLE: Carrera 
 */

ALTER TABLE Carrera ADD CONSTRAINT RefFacultad1 
    FOREIGN KEY (FacultadID)
    REFERENCES Facultad(FacultadID)
go


/* 
 * TABLE: Docente 
 */

ALTER TABLE Docente ADD CONSTRAINT RefPersona4 
    FOREIGN KEY (PersonaID)
    REFERENCES Persona(PersonaID)
go


/* 
 * TABLE: Empleado 
 */

ALTER TABLE Empleado ADD CONSTRAINT RefPersona2 
    FOREIGN KEY (PersonaID)
    REFERENCES Persona(PersonaID)
go

ALTER TABLE Empleado ADD CONSTRAINT RefCargo5 
    FOREIGN KEY (CargoID)
    REFERENCES Cargo(CargoID)
go


/* 
 * TABLE: Estudiante 
 */

ALTER TABLE Estudiante ADD CONSTRAINT RefPersona3 
    FOREIGN KEY (PersonaID)
    REFERENCES Persona(PersonaID)
go


/* 
 * TABLE: EstudianteCarrera 
 */

ALTER TABLE EstudianteCarrera ADD CONSTRAINT RefEstudiante19 
    FOREIGN KEY (EstudianteID)
    REFERENCES Estudiante(EstudianteID)
go

ALTER TABLE EstudianteCarrera ADD CONSTRAINT RefCarrera20 
    FOREIGN KEY (CarreraID)
    REFERENCES Carrera(CarreraID)
go

ALTER TABLE EstudianteCarrera ADD CONSTRAINT RefPlanCarrera21 
    FOREIGN KEY (PlanCarreraID)
    REFERENCES PlanCarrera(PlanCarreraID)
go

ALTER TABLE EstudianteCarrera ADD CONSTRAINT RefFacultad22 
    FOREIGN KEY (FacultadID)
    REFERENCES Facultad(FacultadID)
go


/* 
 * TABLE: Grupo 
 */

ALTER TABLE Grupo ADD CONSTRAINT RefPeriodoDetalle11 
    FOREIGN KEY (PeriodoDetalleID)
    REFERENCES PeriodoDetalle(PeriodoDetalleID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefPeriodo12 
    FOREIGN KEY (PeriodoID)
    REFERENCES Periodo(PeriodoID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefPlanCarrera13 
    FOREIGN KEY (PlanCarreraID)
    REFERENCES PlanCarrera(PlanCarreraID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefCarrera14 
    FOREIGN KEY (CarreraID)
    REFERENCES Carrera(CarreraID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefFacultad15 
    FOREIGN KEY (FacultadID)
    REFERENCES Facultad(FacultadID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefAsignatura16 
    FOREIGN KEY (AsignaturaID)
    REFERENCES Asignatura(AsignaturaID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefHorario17 
    FOREIGN KEY (HorarioID)
    REFERENCES Horario(HorarioID)
go

ALTER TABLE Grupo ADD CONSTRAINT RefDocente18 
    FOREIGN KEY (DocenteID)
    REFERENCES Docente(DocenteID)
go


/* 
 * TABLE: Notas 
 */

ALTER TABLE Notas ADD CONSTRAINT RefGrupo23 
    FOREIGN KEY (GrupoID)
    REFERENCES Grupo(GrupoID)
go

ALTER TABLE Notas ADD CONSTRAINT RefEstudianteCarrera24 
    FOREIGN KEY (EstudianteCarreraID)
    REFERENCES EstudianteCarrera(EstudianteCarreraID)
go


/* 
 * TABLE: PeriodoDetalle 
 */

ALTER TABLE PeriodoDetalle ADD CONSTRAINT RefPeriodo9 
    FOREIGN KEY (PeriodoID)
    REFERENCES Periodo(PeriodoID)
go


/* 
 * TABLE: Persona 
 */

ALTER TABLE Persona ADD CONSTRAINT RefTipoIdentidad6 
    FOREIGN KEY (TipoIdentidadID)
    REFERENCES TipoIdentidad(TipoIdentidadID)
go


/* 
 * TABLE: PlanCarrera 
 */

ALTER TABLE PlanCarrera ADD CONSTRAINT RefCarrera7 
    FOREIGN KEY (CarreraID)
    REFERENCES Carrera(CarreraID)
go

ALTER TABLE PlanCarrera ADD CONSTRAINT RefAsignatura8 
    FOREIGN KEY (AsignaturaID)
    REFERENCES Asignatura(AsignaturaID)
go

ALTER TABLE PlanCarrera ADD CONSTRAINT RefModalidad10 
    FOREIGN KEY (ModalidadID)
    REFERENCES Modalidad(ModalidadID)
go

ALTER TABLE PlanCarrera ADD CONSTRAINT RefAsignatura25 
    FOREIGN KEY (PrerrequisitoAsignatura)
    REFERENCES Asignatura(AsignaturaID)
go


